<?php
/**
 * @author Matthieu Deutscher <contact@dmconcept.fr>
 */

namespace vendor_crypto;

use PDO;

class Db {

    /**
     * @var PDO
     */
    private static $instance;

    /**
     * Singleton
     */
    private function __construct() {

    }

    /**
     *
     * @return PDO
     */
    public static function getInstance() {
        if(!(self::$instance instanceof PDO)) {
            self::init();
        }

        return self::$instance;
    }

    /**
     * Initialisation of PDO instance
     */
    private static function init() {
        $params = Config::getInstance()->getBlock('database');

        $dsn = 'mysql:host='.$params['host'].';dbname='.$params['database'];
        $username = $params['user'];
        $password = $params['password'];
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );

        self::$instance = new PDO($dsn, $username, $password, $options);
    }
}
