<?php

namespace vendor_crypto;

use PDO;

class Annonces {

    private static $categories = array(
        'fichier',
        'donnee',
        'lots'
    );


    protected static function savePhoto($photo, $id_crypto) {
        $sql = "INSERT INTO `crypto_annonces_fichiers` "
             . "SET `reference`  = :reference, "
             . "    `indice` = :indice, "
             //. "    `fichier_joint` = :fichier_joint, "
             . "    `titre_fr` = :titre_fr, "
             . "    `description_fr` = :description_fr, "
             . "    `titre_en` = :titre_en, "
             . "    `description_en` = :description_en, "
             . "    `nom_fichier` = :nom_fichier, "
             . "    `type_mime` = :type_mime, "
             . "    `id_crypto`= :id_crypto";
        $prepare = Db::getInstance()->prepare($sql);

        $parameters = array(
            'reference'  => $photo['REFERENCE'],
            'indice' => $photo['INDICE'],
            'titre_fr' => (is_array($photo['TEXTES']['TITRE_FR'])) ? '' : $photo['TEXTES']['TITRE_FR'],
            'titre_en' => (is_array($photo['TEXTES']['TITRE_EN'])) ? '' : $photo['TEXTES']['TITRE_EN'],
            'description_fr' => (is_array($photo['TEXTES']['DESCRIPTION_FR'])) ? '' : $photo['TEXTES']['DESCRIPTION_FR'],
            'description_en' => (is_array($photo['TEXTES']['DESCRIPTION_EN'])) ? '' : $photo['TEXTES']['DESCRIPTION_EN'],
            'nom_fichier' => $photo['FICHIER_JOINT']['NOM_FICHIER'],
            'type_mime' => $photo['FICHIER_JOINT']['TYPE_MIME'],
            'id_crypto'=> $id_crypto
        );


        if($prepare->execute($parameters)) {
            // Copy images from url to local
            //self::copyPhoto($photo['url'], $photo['name'], $id_crypto);
        } else {
            Log4us::getInstance()->critical(
                "Problem insert image in database ".$id_crypto,
                array('sql' => $sql, 'params' => $parameters, "PDO errors" => $prepare->errorInfo())
            );
        }
    }

    /**
     * Save photos of easy2pilot
     *
     * @param array $photos
     * @param int $id_crypto
     */
    protected static function savePhotos($annonce, $id_crypto) {
        // Delete all picture of id_crypto first
        /**
         * @todo A revoir
         */
        //self::deletePhotos($id_crypto);

        if(isset($annonce['FICHIER'])) {
            $photos = $annonce['FICHIER'];

            if(isset($photos['REFERENCE'])) {
                return self::savePhoto($photos, $id_crypto);
            }

            foreach ($photos as $i => $photo) {
               self::savePhoto($photo, $id_crypto);
            }
        }

    }

    /**
     * Truncate all annonces from databases
     */
    public static function truncate() {
        $sql = "SELECT `id_crypto` FROM `crypto_annonces`";
        $query = Db::getInstance()->query($sql);

        // Delete annonce
        foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $annonce) {
            $id_crypto = (int)$annonce['id_crypto'];
            self::delete($id_crypto);
        }
    }

    /**
     * Delete the whole annonce
     * with pictures
     *
     * @param int $id_crypto
     */
    public static function delete($id_crypto) {
        self::deleteAnnonce($id_crypto);

        foreach (self::$categories as $table) {
            self::deleteAnnonce($id_crypto, 'crypto_annonces_'.$table);
            self::deleteRelativeLots($id_crypto);
        }

        //self::deletePhotos($id_crypto);
    }

    /**
     *
     * @param int $id_crypto
     * @param string $table
     * @return boolean
     */
    protected static function deleteAnnonce($id_crypto, $table = 'crypto_annonces') {
        $sql = "DELETE FROM `".$table."` WHERE `id_crypto` = ".(int)$id_crypto;
        return Db::getInstance()->exec($sql);
    }

    /**
     *
     * @param int $id_crypto
     * @param string $table
     * @return boolean
     */
    protected static function deleteRelativeLots($id_crypto) {
        $sql = "DELETE FROM `crypto_annonces_lots` WHERE `id_crypto_parent` = ".(int)$id_crypto;
        return Db::getInstance()->exec($sql);
    }

    /**
     *
     * @param type $annonce
     * @return type
     */
    protected static function sanytizeAnnonce($annonce) {
        $return = array();
        foreach ($annonce as $key => $value) {
            $key_save = self::epureKey($key);
            if(!in_array($key_save, array_merge(array('idref'), self::$categories))) {
                $key_save = ($key_save === 'idref') ? "id_crypto" : $key_save;
                $key_save = ($key_save === 'idimmeuble') ? "id_crypto_parent" : $key_save;
                $value = (is_array($value)) ? '' : $value;
                $return[$key_save] = $value;
            }
        }

        return $return;
    }

    /**
     *
     * @param type $annonce
     */
    protected static function saveLots($annonce) {
        //var_dump($annonce['LOTS']);
        foreach ($annonce['LOTS'] as $lots) {
            if(!isset($lots['LOT_IDREF'])) {
                foreach ($lots as $lot) {
                    $id_crypto = (int)$lot['LOT_IDREF'];
                    self::saveAnnonce(self::sanytizeAnnonce($lot), $id_crypto, "crypto_annonces_lots");
                }
            } else {
                $id_crypto = (int)$lots['LOT_IDREF'];
                self::saveAnnonce(self::sanytizeAnnonce($lots), $id_crypto, "crypto_annonces_lots");
            }
        }
    }

    /**
     *
     * @param array $annonces
     */
    public static function saveAnnonces($annonces) {
        $parameters = Config::getInstance()->getBlock('parameters');
        $ignore_photos = (bool)$parameters['ignore_photos'];

        // Save to database with another class
        if(is_array($annonces) && count($annonces) > 0) {
            foreach ($annonces as $i => $annonce) {

                //var_dump($annonce);

                $id_crypto = (int)$annonce['IM_IDREF'];
                self::saveAnnonce(self::sanytizeAnnonce($annonce), $id_crypto, "crypto_annonces");
                self::saveLots($annonce, $id_crypto);

                if(!$ignore_photos) {
                    self::savePhotos($annonce, $id_crypto);
                }
            }
        }
    }

    /**
     *
     * @param type $annonce
     * @return type
     */
    public static function save($annonce) {
        return self::saveAnnonces(array($annonce));
    }

    /**
     *
     * @param type $annonce
     * @param type $id_crypto
     * @param type $table
     * @return boolean
     */
    protected static function saveAnnonce($annonce, $id_crypto, $table = 'crypto_annonces') {
        Log4us::getInstance()->info("Save ".$table." for ".$id_crypto);
        self::deleteAnnonce($id_crypto, $table);

        $parameters = array('id_crypto' => $id_crypto);
        $sql = 'INSERT INTO `'.$table.'` SET `id_crypto` = :id_crypto ';
        unset($annonce['id']);
        foreach ($annonce as $key => $value) {
            $parameters[$key] = $value;
            $sql .= ", `".$key."` = :".$key;
        }
        //echo count($parameters)." -- ".$sql."<br /><br />";

        $prepare = Db::getInstance()->prepare($sql);
        if(!$prepare->execute($parameters)) {
            Log4us::getInstance()->critical(
                    "Problem insert ".$table." in database ".$id_crypto,
                    array('sql' => $sql, 'params' => $parameters, "PDO errors" => $prepare->errorInfo())
            );
        }

        return true;
    }

    /**
     * Epure Key
     * @param string $key
     * @return string
     */
    protected static function epureKey($key) {
        $return = strtolower($key);
        $return = str_replace('im_', '', $return);
        $return = str_replace('lot_', '', $return);

        return $return;
    }

    /**
     * Only to create table to avoid insert column one by one in database
     *
     * @deprecated since version 1.0
     * @param array $annonce
     */
    public static function generateColumnTable($annonce) {
        /**
         * Create Annonce
         */
        $sql = 'CREATE TABLE `crypto_annonces` ( 
                   `id` INT(11) NOT NULL AUTO_INCREMENT , 
                   `id_crypto` INT(11) NOT NULL , 
                   PRIMARY KEY (`id`), 
                   INDEX (`id_crypto`)) ENGINE = InnoDB;';
        echo $sql."<br />";

        // idref = id_crypto
        $ignore_key = array('donnee', 'fichier', 'lots', 'idref');
        foreach ($annonce as $key => $datas) {
            if(!in_array(self::epureKey($key), $ignore_key)) {
                echo "ALTER TABLE `crypto_annonces` ADD `".self::epureKey($key)."` TEXT NULL;<br />";
            }
        }

        /***
         * Create lot
         */
        $sql = 'CREATE TABLE `crypto_annonces_lots` ( 
                   `id` INT(11) NOT NULL AUTO_INCREMENT , 
                   `id_crypto` INT(11) NOT NULL, 
                   `id_crypto_parent` INT(11) NOT NULL, 
                   PRIMARY KEY (`id`), 
                   INDEX (`id_crypto`)) ENGINE = InnoDB;';
        echo $sql."<br />";

        //var_dump($annonce['LOTS']['LOT']);
        foreach ($annonce['LOTS']['LOT'][0] as $key => $datas) {
            if(!in_array(self::epureKey($key), $ignore_key)) {
                echo "ALTER TABLE `crypto_annonces_lots` ADD `".self::epureKey($key)."` TEXT NULL;<br />";
            }
        }

        /***
         * Create lot
         */
        $sql = 'CREATE TABLE `crypto_annonces_fichiers` ( 
                   `id` INT(11) NOT NULL AUTO_INCREMENT , 
                   `id_crypto` INT(11) NOT NULL, 
                   PRIMARY KEY (`id`), 
                   INDEX (`id_crypto`)) ENGINE = InnoDB;';
        echo $sql."<br />";
        //var_dump($annonce['FICHIER']);
        foreach ($annonce['FICHIER'][0] as $key => $datas) {
            if(!in_array(self::epureKey($key), array('textes'))) {
                echo "ALTER TABLE `crypto_annonces_fichiers` ADD `".self::epureKey($key)."` TEXT NULL;<br />";
            }
        }

        foreach ($annonce['FICHIER'][0]['TEXTES'] as $key => $datas) {
            if(!in_array(self::epureKey($key), array('textes'))) {
                echo "ALTER TABLE `crypto_annonces_fichiers` ADD `".self::epureKey($key)."` TEXT NULL;<br />";
            }
        }

        foreach ($annonce['FICHIER'][0]['FICHIER_JOINT'] as $key => $datas) {
            if(!in_array(self::epureKey($key), array('textes'))) {
                echo "ALTER TABLE `crypto_annonces_fichiers` ADD `".self::epureKey($key)."` TEXT NULL;<br />";
            }
        }

    }

    /**
     * Copy image from url to local
     *
     * @param string $url
     * @param string $name
     * @param int $id_crypto
     */
    protected static function copyPhoto($url, $name, $id_crypto) {
        Log4us::getInstance()->info("Call copyPhoto for ".$id_crypto." --> Url: ".$url );
        $parameters = Config::getInstance()->getBlock('parameters');
        $path_img = $parameters['path_img'];

        $content = file_get_contents($url);

        $directory = $path_img.'/'.(int)$id_crypto.'/';
        if(!is_dir($directory)) {
            Log4us::getInstance()->info("Create directory image for ".$id_crypto);
            mkdir($directory);
        }

        file_put_contents($directory.$name, $content);
    }

    /**
     * Delete photos for a specific id_crypto
     *
     * @todo à refaire
     * @param int $id_crypto
     * @return boolean
     */
    public static function deletePhotos($id_crypto) {
        $parameters = Config::getInstance()->getBlock('parameters');
        $path_img = $parameters['path_img'];

        $sql = "SELECT `id`, `nom_fichier` FROM `crypto_annonces_fichiers` WHERE `id_crypto` = ".(int)$id_crypto;
        $query = Db::getInstance()->query($sql);

        // Delete file inside the directory
        foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $photo) {
            $file = $path_img.'/'.(int)$id_crypto.'/'.$photo['name'];
            if(file_exists($file)) {
                Log4us::getInstance()->info("Delete image for ".$id_crypto." -- ".$file);
                unlink($file);
            }
        }

        // Delete directory
        if(is_dir($path_img.(int)$id_crypto.'/')) {
            Log4us::getInstance()->info("Delete directory image for ".$id_crypto);
            rmdir($path_img.(int)$id_crypto.'/');
        }

        // Delete in database the entry
        $sql = "DELETE FROM `crypto_annonces_fichiers` WHERE `id_crypto` = ".(int)$id_crypto;
        return Db::getInstance()->exec($sql);
    }
}
