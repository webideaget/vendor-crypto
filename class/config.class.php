<?php
// YAML Parser
namespace vendor_crypto;

use Symfony\Component\Yaml\Yaml;
class Config {
    
    /**
     *
     * @var array 
     */
    private $config = null;
    
    /**
     * Instance of Config
     * @var Config
     */
    private static $_instance = null; 
    
    /**
     *
     * @var string 
     */
    private static $configFile = '../config/parameters.yml';
    
    /**
     * Private construct
     */
    private function __construct() {
        $this->config = Yaml::parse(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . self::$configFile));
    }
    
    /**
     * Get instance of this class
     * 
     * @return type
     */
    public static function getInstance() {
        if(self::$_instance === null) {
            self::$_instance = new Config();
        }
        
        return self::$_instance;
    }
    
    /**
     * Return list of parameters
     * 
     * @return array
     */
    public function getAll() {
        return $this->config;
    }
    
    /**
     * Return a list of key / value depending of $key
     * 
     * @param string $key
     * @return mixed
     */
    public function getBlock($key) {
        if(isset($this->config[$key])) {
            return $this->config[$key];
        }
        
        return false;
    }
    
} 