<?php
// Include all files and autoload needed for this project

require_once dirname(__FILE__).'/../../../autoload.php';

// General import
include dirname(__FILE__).'/config.class.php';
include dirname(__FILE__).'/db.class.php';
include dirname(__FILE__).'/annonces.class.php';
include dirname(__FILE__).'/log4us.class.php';

// Crypto Import
include dirname(__FILE__).'/crypto/crypto.class.php';
include dirname(__FILE__).'/crypto/webservices.class.php';
