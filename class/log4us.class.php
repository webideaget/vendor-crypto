<?php

namespace vendor_crypto;

use Monolog\Handler\SwiftMailerHandler as SwiftMailerHandler;
use Monolog\Logger;
use Swift_Message;
use Swift_Mailer;
use Swift_SmtpTransport;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\HtmlFormatter;

class Log4us extends Logger {

    /**
     * Instance of Monolog
     * @var Log4us
     */
    private static $_instance = null;

    /**
     *
     * @var array
     */
    protected $params;

    /**
     * Return current instance
     * @return Log4us
     */
    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new Log4us();
        }

        return self::$_instance;
    }

    /**
     * Private Constructor
     */
    public function __construct() {
        $this->params = Config::getInstance()->getBlock('logs');
        parent::__construct($this->params['channel']);

        // Create transporter for critical problem
       //$this->createTransporterEmail();

        // Create transporter for Info problem
        $this->createTransporterLog();
    }

    /**
     * Create Transporter Log File
     */
    protected function createTransporterLog() {
        $stream = new StreamHandler(
                dirname(__FILE__).'/../'.$this->params['info']['directory'].'/infos.log',
                Logger::DEBUG);
        $this->pushHandler($stream);
    }

    /**
     * Create Transporter of email
     */
    protected function createTransporterEmail() {
        $params_email = Config::getInstance()->getBlock('email');

        // Create the Transport for critical
        $transporter = new Swift_SmtpTransport(
                $params_email['host'],
                $params_email['port'],
                $params_email['security']);

        $transporter->setUsername($params_email['user']);
        $transporter->setPassword($params_email['password']);

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transporter);

        // Create a message
        $message = (new Swift_Message($this->params['critical']['title']));
        $message->setFrom([$this->params['critical']['from']]);
        $message->setTo([$this->params['critical']['to']]);
        $message->setContentType("text/html");

        $mailerHandler = new SwiftMailerHandler($mailer, $message, Logger::CRITICAL);
        $mailerHandler->setFormatter(new HtmlFormatter());
        $this->pushHandler($mailerHandler);
    }
}
