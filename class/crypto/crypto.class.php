<?php
/**
 * @author Matthieu Deutscher <contact@dmconcept.fr>
 * @version 1.0.0
 */
namespace vendor_crypto;

use SimpleXMLElement;

class crypto {
    /**
     * Array of the parameters
     * @var array
     */
    protected $config = array();

    /**
     * Debug true / false
     * @var boolean
     */
    protected $debug;

    /**
     * Token given by easy2pilot API
     * @var string
     */
    protected $token;

    /**
     * Construct with file parameters
     *
     * @param type $configFile
     */
    public function __construct() {
        // Parse parameters to get in config variable
        $this->parseParameters();
    }

    /**
     * Get All parameters from Config
     */
    protected function parseParameters() {
        $this->config = Config::getInstance()->getBlock('crypto');

        // debug
        $this->debug = (bool)Config::getInstance()->getBlock('debug');
    }

    /**
     * Return list of real estate
     * @return mixed
     */
    public function getAnnonces() {
        // Work !
        if(!$this->debug) {
            Log4us::getInstance()->info("Debug mode is not activated: Find XML in \"depot\"");
            /**
             * @todo: Changer localisation
             */
        } else {
            Log4us::getInstance()->info("Debug mode is activated");
            // Return a JSON --> to avoid call X times for tests
        }

        $datas = file_get_contents(dirname(__FILE__).'/Export_Office.xml');

        try {
            $xml = new SimpleXMLElement($datas);
            $datas = json_decode(json_encode($xml), true);
            Log4us::getInstance()->info("GetAnnonces from crypto");

            return $datas['IMMEUBLE'];
        } catch (Exception $e) {
            Log4us::getInstance()->critical("GetAnnonces from crypto problem", $datas);
        }

        return false;
    }

}
