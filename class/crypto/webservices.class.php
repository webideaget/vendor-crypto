<?php
/**
 * Call the API
 */
namespace vendor_crypto;

class Webservices {
    
    /**
     * Instance of crypto
     * @var crypto 
     */
    private $crypto;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->crypto = new crypto();
        
    }
    
    /**
     * @todo: See for FTP import ???
     */
    
    /**
     * Synchrionisze all annonces
     */
    public function syncAllAnnonces() {
        Log4us::getInstance()->info("Call syncAllAnnonces");
        $annonces = $this->crypto->getAnnonces();
        
       
        //Annonces::generateColumnTable($annonces[1]);
        
        if(is_array($annonces) && count($annonces) > 0) {
            // Delete all annonces
            Annonces::truncate();
            
            // Save all annonce
            Annonces::saveAnnonces($annonces);
        }
    }
    
}