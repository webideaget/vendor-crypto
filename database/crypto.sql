
--
-- Structure de la table `annonces`
--

CREATE TABLE `crypto_annonces` (
  `id` int(11) NOT NULL,
  `id_crypto` int(11) NOT NULL,
  `timestamp` TEXT,
  `aqui` TEXT,
  `code` TEXT,
  `adr1` TEXT,
  `adr2` TEXT,
  `cp` TEXT,
  `ville` TEXT,
  `adresseimmeuble` TEXT,
  `pays` TEXT,
  `secteurgeo` TEXT,
  `dateachvtvx` TEXT,
  `nopermisconstr` TEXT,
  `datecertifconform` TEXT,
  `assujettitva` TEXT,
  `noteassurance` TEXT,
  `noteregdecopro` TEXT,
  `noteimpot` TEXT,
  `noteparticularite` TEXT,
  `repartdu` TEXT,
  `repartau` TEXT,
  `repartperiodique` TEXT,
  `identite` TEXT,
  `datederniereag` TEXT,
  `noteag` TEXT,
  `dateprochaineag` TEXT,
  `heureag` TEXT,
  `lieuag` TEXT,
  `dateexpedconvoc` TEXT,
  `m_frais1` TEXT,
  `m_frais2` TEXT,
  `m_frais3` TEXT,
  `m_fraisplus3` TEXT,
  `engestioncomplete` TEXT,
  `codestatistique` TEXT,
  `ppr` TEXT,
  `declarationsinistre` TEXT,
  `codecompta` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `annonces`
--
ALTER TABLE `crypto_annonces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_crypto` (`id_crypto`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `annonces`
--
ALTER TABLE `crypto_annonces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



--
-- Structure de la table `annonces_lots`
--

CREATE TABLE `crypto_annonces_lots` (
  `id` int(11) NOT NULL,
  `id_crypto` int(11) NOT NULL,
  `id_crypto_parent` int(11) NOT NULL,
  `timestamp` TEXT,
  `aqui` TEXT,
  `codelot` TEXT,
  `idimmeuble` TEXT,
  `idappartient` TEXT,
  `etatlocatif` TEXT,
  `dateliberation` TEXT,
  `etatdulot` TEXT,
  `idpropgerance` TEXT,
  `idpropsyndic` TEXT,
  `idproptransac` TEXT,
  `idpropsaison` TEXT,
  `critere1` TEXT,
  `critere2` TEXT,
  `critere3` TEXT,
  `critere4` TEXT,
  `critere5` TEXT,
  `qualif1` TEXT,
  `qualif2` TEXT,
  `qualif3` TEXT,
  `qualif4` TEXT,
  `qualif5` TEXT,
  `surface1` TEXT,
  `surface2` TEXT,
  `surface3` TEXT,
  `surface4` TEXT,
  `surfacetotale` TEXT,
  `batiment` TEXT,
  `escalier` TEXT,
  `etage` TEXT,
  `porte` TEXT,
  `digicode` TEXT,
  `nombrepieces` TEXT,
  `noparking` TEXT,
  `typepkg` TEXT,
  `rubnum1` TEXT,
  `rubnum2` TEXT,
  `rubnum3` TEXT,
  `notetransaction` TEXT,
  `titreannonce` TEXT,
  `descriptionpub` TEXT,
  `secteurgeo` TEXT,
  `dateparutionannonce` TEXT,
  `periodicite` TEXT,
  `m_loyerht` TEXT,
  `idtaxeloyer` TEXT,
  `m_provchargesht` TEXT,
  `taxessurprovision` TEXT,
  `m_loyerm2ht` TEXT,
  `m_provm2ht` TEXT,
  `exoneretva` TEXT,
  `liberationexoneration` TEXT,
  `idcodetaquittancee` TEXT,
  `idtxabattrf` TEXT,
  `honolocataire` TEXT,
  `idtaxehonoloc` TEXT,
  `honoproprietaire` TEXT,
  `idtaxehonoprop` TEXT,
  `txoumontant` TEXT,
  `m_chauffage` TEXT,
  `m_electricite` TEXT,
  `notevisite` TEXT,
  `notelocation` TEXT,
  `dateconstruction` TEXT,
  `m_prixvente` TEXT,
  `m_prixnetvendeur` TEXT,
  `m_montantcommission` TEXT,
  `idtaxecommission` TEXT,
  `commissionacheteur` TEXT,
  `m_prixventem2` TEXT,
  `txcommission` TEXT,
  `diffusercetteannonce` TEXT,
  `imagemodif` TEXT,
  `adr1` TEXT,
  `adr2` TEXT,
  `cp` TEXT,
  `ville` TEXT,
  `m_taxehab` TEXT,
  `m_tom` TEXT,
  `m_foncier` TEXT,
  `m_chargescopro` TEXT,
  `m_dontrecup` TEXT,
  `numerorcp` TEXT,
  `numeroappartmt` TEXT,
  `refinternet` TEXT,
  `refcles` TEXT,
  `nomenclature` TEXT,
  `surfacecarrez` TEXT,
  `coderegrouppement` TEXT,
  `lotsecondaire` TEXT,
  `notetariffr` TEXT,
  `notetarifus` TEXT,
  `codetarifsaisonnier` TEXT,
  `codelienautreappli` TEXT,
  `dpe_isolation_classe` TEXT,
  `typebieninternet` TEXT,
  `m_dgsaisonnier` TEXT,
  `nbcouchages` TEXT,
  `notesaisonnier` TEXT,
  `titreannonceus` TEXT,
  `descriptifpubus` TEXT,
  `gps1` TEXT,
  `gps2` TEXT,
  `adresse_internet` TEXT,
  `confidentialite` TEXT,
  `codelotgestion` TEXT,
  `date_nondispo` TEXT,
  `note_nondispo` TEXT,
  `titreannoncevitrine` TEXT,
  `descriptionvitrine` TEXT,
  `titreannonceparution` TEXT,
  `descriptionparution` TEXT,
  `coupdecoeur` TEXT,
  `divisibleapartirde` TEXT,
  `publicationsaisonnier` TEXT,
  `idinterlocappartient` TEXT,
  `ville_internet` TEXT,
  `comptepi` TEXT,
  `historisationprix` TEXT,
  `conventionne` TEXT,
  `convention` TEXT,
  `conventiondate` TEXT,
  `conventiondateeffet` TEXT,
  `conventiondateecheance` TEXT,
  `conventionsurface` TEXT,
  `m_conventionplafond` TEXT,
  `conventionzone` TEXT,
  `quitt_chargescopro` TEXT,
  `cp_internet` TEXT,
  `pays_internet` TEXT,
  `dpe_energie_classe` TEXT,
  `dpe_energie_valeur` TEXT,
  `dpe_climat_classe` TEXT,
  `dpe_climat_valeur` TEXT,
  `premieroccupant` TEXT,
  `m_foncierm2` TEXT,
  `dpe_isolation_valeur` TEXT,
  `motif_nondispo` TEXT,
  `dpe_nonconcerne` TEXT,
  `dpe_date` TEXT,
  `conventionnumero` TEXT,
  `situation_plan_lot` TEXT,
  `descriptionpub_location` TEXT,
  `descriptionpubus_location` TEXT,
  `titreannonce_location` TEXT,
  `titreannonceus_location` TEXT,
  `descriptionvitrineus` TEXT,
  `titreannoncevitrineus` TEXT,
  `lotcumulableimmeuble` TEXT,
  `dpe_vierge` TEXT,
  `dpe_typetertiaire` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `annonces_lots`
--
ALTER TABLE `crypto_annonces_lots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_crypto` (`id_crypto`),
  ADD KEY `id_crypto_parent` (`id_crypto_parent`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `annonces_lots`
--
ALTER TABLE `crypto_annonces_lots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `annonces_lots`
--
ALTER TABLE `crypto_annonces_lots`
  ADD CONSTRAINT `annonces_lots_ibfk_1` FOREIGN KEY (`id_crypto_parent`) REFERENCES `crypto_annonces` (`id_crypto`);



CREATE TABLE `crypto_annonces_fichiers` (
  `id` int(11) NOT NULL,
  `id_crypto` int(11) NOT NULL,
  `reference` TEXT,
  `indice` TEXT,
  `fichier_joint` TEXT,
  `titre_fr` TEXT,
  `description_fr` TEXT,
  `titre_en` TEXT,
  `description_en` TEXT,
  `nom_fichier` TEXT,
  `type_mime` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `annonces_fichiers`
--
ALTER TABLE `crypto_annonces_fichiers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_crypto` (`id_crypto`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `annonces_fichiers`
--
ALTER TABLE `crypto_annonces_fichiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `annonces_fichiers`
--
ALTER TABLE `crypto_annonces_fichiers`
  ADD CONSTRAINT `annonces_fichiers_ibfk_1` FOREIGN KEY (`id_crypto`) REFERENCES `crypto_annonces` (`id_crypto`);