<?php
namespace vendor_crypto;
require_once '../../autoload.php';

// Instantiate PHP class
$webservices = new Webservices();

// Synchronisation of all annonces
$webservices->syncAllAnnonces();

// Synchronisation of one Annonce
//$webservices->updateAnnonce('2522');

// ynchronisation of one Annonce --> Delete
//$webservices->deleteAnnonce('2522');
